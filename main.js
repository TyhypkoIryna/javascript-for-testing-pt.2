const {City} = require('./City');
const {Capital} = require('./Capital');
const {Country} = require('./Country');


let city1 = new City('Kharkiv')
let city2 = new City('Lviv')

//city1.setWeather().then(() => {console.log(JSON.stringify(city1))}).catch((error) => {console.log(error)})
//city2.setWeather().then(() => {console.log(JSON.stringify(city2))}).catch((error) => {console.log(error)})

let capital = new Capital('Kyiv')
capital.setAirport('Boryspil')
//capital.setWeather().then(() => {console.log(JSON.stringify(capital))}).catch((error) => {console.log(error)})

const newCities = [
    'London',
    'Birmingham',
    'Leeds',
    'Salford',
    'Chester',
    'Derby',
    'Glasgow',
    'Liverpoo',
];

const mainHomework = async () =>  {

    let england = new Country('England', [city1, city2]);

    newCities.forEach( cityName => england.addCity(cityName));

    await england.setWeather();

    england.deletCity('Salford');

    england.sortCitiesByTemperature();

    england.showWeather();
};

mainHomework();
