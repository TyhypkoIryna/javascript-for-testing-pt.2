const {City} = require('./City');

class Country{

    constructor(name, cities = []) {
        this.name = name;
        this.cities = cities [ 'London', 'Leeds', 'Salford'];
    }
 
    deletCity(name) {
        const RemoveIndex = this.cities.findIndex(city => city.name === name);
        this.cities.splice(RemoveIndex, 2);
    }

    async addCity(name){
        await this.cities.push(new City(name));
    }
    
    async setWeather(){
        await city.setWeather(this.cities)}
    
    sortCitiesByTemperature() {
        this.cities.sort( (a,b) => this.getTemperature(b) - this.getTemperature(a));
    }
}


    module.exports = {Country};
